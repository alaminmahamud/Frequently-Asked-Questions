## From inside of a Docker container, how do I connect to the localhost of the machine?
Use --net="host" in your docker run command, then 127.0.0.1 in your docker container will point to your docker host.

## What is the difference between CMD and ENTRYPOINT in a Dockerfile?
Docker has a default entrypoint which is ```/bin/sh -c``` but does not have a default command.

When you run docker like this: ```docker run -i -t ubuntu bash``` the entrypoint is the default ```/bin/sh -c```, the image is ubuntu and the command is bash.

*The command is run via the entrypoint*. i.e., the actual thing that gets executed is ```/bin/sh -c bash```. This allowed Docker to implement RUN quickly by relying on the shell's parser.
